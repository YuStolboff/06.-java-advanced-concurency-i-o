package com.epam.stolbovy.concurency.service;

import com.epam.stolbovy.concurency.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;

public class WriterAccountsInFiles implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(WriterAccountsInFiles.class);

    public void writesAccountInFile(Account account, String path) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(path + account.getId() + "-account.bin");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(account);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (IOException e) {
            log.info("Exception I/O {}", e);
        }
    }

    public void deletesFiles(String path) {
        for (File file : Objects.requireNonNull(new File(path).listFiles())) {
            if (file.isFile()) {
                file.deleteOnExit();
            }
        }
    }
}
