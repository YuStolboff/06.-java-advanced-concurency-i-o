package com.epam.stolbovy.concurency.service;

import com.epam.stolbovy.concurency.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class ReaderAccountsFromFiles {

    private static final Logger log = LoggerFactory.getLogger(ReaderAccountsFromFiles.class);

    private List<String> readsFiles(String path) {
        List<String> filesList = new ArrayList<>();
        try {
            File pathDirectory = new File(path);
            filesList = Arrays.asList(Objects.requireNonNull(pathDirectory.list()));
        } catch (NullPointerException e) {
            log.info("NullPointerException {}", e);
        }
        return filesList;
    }

    public List<Account> readsAccounts(String path) {
        List<String> filesList = readsFiles(path);
        List<Account> accountList = new ArrayList<>();
        try {
            for (String file : filesList) {
                FileInputStream fileInputStream = new FileInputStream(path + file);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                Account account = (Account) objectInputStream.readObject();
                accountList.add(account);
            }
        } catch (IOException | ClassNotFoundException e) {
            log.info("Exception {}", e);
        }
        return accountList;
    }
}
