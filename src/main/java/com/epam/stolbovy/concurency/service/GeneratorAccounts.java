package com.epam.stolbovy.concurency.service;

import com.epam.stolbovy.concurency.Account;

import java.util.Random;

public class GeneratorAccounts {

    public Account generatesAccount(int id) {
        Random random = new Random();
        return new Account((5000 + random.nextInt(5000)), id);
    }
}
