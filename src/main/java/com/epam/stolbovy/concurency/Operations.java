package com.epam.stolbovy.concurency;

import com.epam.stolbovy.concurency.service.GeneratorAccounts;
import com.epam.stolbovy.concurency.service.ReaderAccountsFromFiles;
import com.epam.stolbovy.concurency.service.WriterAccountsInFiles;
import com.epam.stolbovy.concurency.utilites.Transfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Operations {

    private static final int OPERATIONS_NUMBER = 100;
    private static final int ACCOUNTS_NUMBER = 20;
    private static final int MAX_TRANSFER = 9000;
    private static final int MAX_THREADS = 10;
    private static final String PATH = "accounts\\";
    private static final Logger log = LoggerFactory.getLogger(Operations.class);

    public static void main(String[] args) {
        WriterAccountsInFiles writerAccountsInFiles = new WriterAccountsInFiles();
        writerAccountsInFiles.deletesFiles(PATH);
        ReaderAccountsFromFiles readerAccountsFromFiles = new ReaderAccountsFromFiles();
        int sumOfAllMoney;
        int accountIdFirst;
        int accountIdSecond;

        for (int i = 0; i < ACCOUNTS_NUMBER; i++) {
            writerAccountsInFiles.writesAccountInFile(new GeneratorAccounts().generatesAccount(i), PATH);
        }
        log.info("Accounts writes in files");
        List<Account> accountList = readerAccountsFromFiles.readsAccounts(PATH);
        log.info("From files read accounts and put them in list");
        log.info("Beginning accounts list: {}", accountList);
        sumOfAllMoney = (int) accountList.stream().mapToLong(Account::getBalance).sum();
        log.info("Starting amount of all money: {}", sumOfAllMoney);

        ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREADS);
        for (int i = 0; i < OPERATIONS_NUMBER; i++) {
            Random random = new Random();
            accountIdFirst = random.nextInt(ACCOUNTS_NUMBER);
            accountIdSecond = random.nextInt(ACCOUNTS_NUMBER);
            executorService.submit(new Transfer(accountList.get(accountIdFirst),
                    accountList.get(accountIdSecond), random.nextInt(MAX_TRANSFER)));
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.info("InterruptedException {}", e);
        }

        log.info("Final accounts list: {}", accountList);
        sumOfAllMoney = (int) accountList.stream().mapToLong(Account::getBalance).sum();
        log.info("Final amount of all money: {}", sumOfAllMoney);
        for (Account account : accountList) {
            writerAccountsInFiles.writesAccountInFile(account, PATH);
        }
        log.info("All operations completed");
    }
}
