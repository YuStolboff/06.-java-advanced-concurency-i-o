package com.epam.stolbovy.concurency.utilites;

import com.epam.stolbovy.concurency.Account;
import com.epam.stolbovy.concurency.exceptions.InsufficientFundsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class Transfer implements Callable<Boolean> {

    private final Account accountFirst;
    private final Account accountSecond;
    private final long amount;

    public Transfer(Account accountFirst, Account accountSecond, long amount) {
        this.accountFirst = accountFirst;
        this.accountSecond = accountSecond;
        this.amount = amount;
    }

    private static final Logger log = LoggerFactory.getLogger(Transfer.class);
    private static final long WHITE_SECONDS = 1L;

    @Override
    public Boolean call() throws InterruptedException {
        Thread.sleep(500);
        if (accountFirst.getLock().tryLock(WHITE_SECONDS, TimeUnit.SECONDS)) {
            if (accountSecond.getLock().tryLock(WHITE_SECONDS, TimeUnit.SECONDS)) {
                try {
                    if (accountFirst.getBalance() < amount) {
                        throw new InsufficientFundsException();
                    }
                    log.info("Account-{}, beginning balance {}", accountFirst.getId(), accountFirst.getBalance());
                    log.info("Account-{}, beginning balance {}", accountSecond.getId(), accountSecond.getBalance());
                    accountFirst.withdraw(amount);
                    accountSecond.deposit(amount);
                    log.info("Account-{}, withdraw {}, final balance {}", accountFirst.getId(), amount , accountFirst.getBalance());
                    log.info("Account-{}, deposit {}, final balance {}", accountSecond.getId(), amount, accountSecond.getBalance());
                    log.info("Transfer from account {}, to account {}, is successful", accountFirst.getId(), accountSecond.getId());
                    return true;
                } catch (InsufficientFundsException e) {
                    log.info("Account-{}, insufficient funds", accountFirst.getId());
                } finally {
                    accountFirst.getLock().unlock();
                    accountSecond.getLock().unlock();
                }
            }
        }
        log.info("Error transfer from account {}, to account {}", accountFirst.getId(), accountSecond.getId());
        return false;
    }
}
