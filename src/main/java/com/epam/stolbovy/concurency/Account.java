package com.epam.stolbovy.concurency;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account implements Serializable {

    private long balance;
    private final int id;
    private final Lock lock;

    public Account(long balance, int id) {
        this.balance = balance;
        this.id = id;
        this.lock = new ReentrantLock();
    }

    public void withdraw(long amount) {
        balance -= amount;
    }

    public void deposit(long amount) {
        balance += amount;
    }

    public long getBalance() {
        return balance;
    }

    public int getId() {
        return id;
    }

    public Lock getLock() {
        return lock;
    }

    @Override
    public String toString() {
        return "Account{" +
                "balance=" + balance +
                ", id=" + id +
                '}';
    }
}
